const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const fetch = require("node-fetch");
//requerimos el index.js de models que inicializa sequelize
const model = require('../models/index');
// importante: todas las rutas get, post... son relativas a la ruta principal
// de este controlador: /api/pokemons
// GET lista de todos los pokemons
// vinculamos la ruta /api/pokemons a la función declarada
// si todo ok devolveremos un objeto tipo:
// {ok: true, data: [lista_de_objetos_pokemon...]}
// si se produce un error:
// {ok: false, error: mensaje_de_error}
router.get('/', function (req, res, next) {
    //findAll es un método de sequelize!
    model.Phrase_es.findAll()
        .then(phrases => res.json({
            ok: true,
            data: phrases
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
});

router.get('/random', function (req, res, next) {
    //findAll es un método de sequelize!
    model.Phrase_es.findAll({
        order: [
            [Sequelize.literal('RAND()')]
        ],
        limit: 1,
    })
        .then((frase) => res.json({
            ok: true,
            data: frase
        }))
});
router.get('/categorias', function (req, res, next) {
    //findAll es un método de sequelize!
    model.Phrase_es.findAll({
        attributes: ["categoria"],
        group: ["categoria"]
    })
        .then(categorias => res.json({
            ok: true,
            data: categorias
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
});
// router.get('/:categoria', function (req, res, next) {
//     model.Phrase_es.findAll({ where: { categoria: req.params.categoria } })
//         // .then(pokemon => pokemon.get({plain: true}))
//         .then(phrase => res.json({
//             ok: true,
//             data: phrase
//         }))
//         .catch(error => res.json({
//             ok: false,
//             error: error
//         }))
// });
// GET de tipo /api/pokemons/7dd
// petición de UN pokemon, con ID=7
// "7" se recibe por parámetro y se lee con req.params.id
// en este caso utilizamos el método sequelize findOne
// con la condición "where id=7"
// expresada en forma de objeto: { where: {id:req.params.id}}
router.get('/:id', function (req, res, next) {
    model.Phrase_es.findOne({ where: { id: req.params.id } })
        // .then(pokemon => pokemon.get({plain: true}))
        .then(phrase => res.json({
            ok: true,
            data: phrase
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
});

router.put('rate/:id', function (req, res, next) {
    model.Phrase_es.findOne({ where: { id: req.params.id } })
        // .then(pokemon => pokemon.get({plain: true}))
        .then(phrase => res.json({
            ok: true,
            data: phrase
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
});

// POST a /api/pokemons creamos un nuevo registro
// en primer lugar creamos un modelo Pokemon con el método sequelize "create"
// a partir de los datos presentes en el "body"
// a continuación, mediante "save", el objeto se guarda automáticamente
// en la base de datos y e actualiza su ID!
// igualmente se devuelve el objeto creado
router.post('/', function (req, res, next) {
    console.log(req.body);
    model.Phrase_es.create(req.body)
        .then((item) => item.save())
        .then((item) => res.json({ ok: true, data: item }))
        .catch((error) => res.json({ ok: false, error }))
});


// router.get('/signo/:signo', function (req, res, next) {

//     let signo = req.params.signo;
//     let fetchUrl = "http://horoscope-api.herokuapp.com/horoscope/today/" + signo;
//     //return new Promise((resolve, reject) => {
//     fetch(fetchUrl)
//         .then(results => results.json())
//         .then(horoscopeData => {
//             console.log("Horoscope Data: " + horoscopeData);
//             res.json({ ok: true, data: horoscopeData })
//         })
//         .catch(error => res.json({ ok: false, error }));
// })





// PUT a /api/pokemons/X
// en primer lugar se localiza el pokemon con id=X en la BDD
// a continuación, mediante "update", el objeto se actualiza con los datos
// presentes en el "body"

/*Recibe objeto {"puntuacion":"4"}*/
router.put('/rate/:id', function (req, res, next) {
    model.Phrase_es.findOne({ where: { id: req.params.id } })
        .then((phrase) => {
            console.log("inicial Rating " + phrase.puntuacionTotal);
            console.log("req puntuacion " + req.body.puntuacionTotal);
            console.log("id " + req.params.id);

            let newPhrase = {
                puntuacionTotal: phrase.puntuacionTotal * 1 + req.body.puntuacion * 1,
                puntuado: phrase.puntuado + 1
            }
            phrase.update(newPhrase)
            return newPhrase;
        }
        )
        .then((ret) => res.json({
            ok: true,
            data: ret
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }));
});




// DELETE a /api/pokemons/X
// se elimina el registro con id = X con elmétodo sequelize "destroy"
router.delete('/:id', function (req, res, next) {
    model.Phrase_es.destroy({ where: { id: req.params.id } })
        .then((data) => res.json({ ok: true, data }))
        .catch((error) => res.json({ ok: false, error }))
});
module.exports = router;