const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const fetch = require("node-fetch");
//requerimos el index.js de models que inicializa sequelize
const model = require('../models/index');



router.get('/:signo', function (req, res, next) {

    let signo = req.params.signo;
    let fetchUrl = "http://horoscope-api.herokuapp.com/horoscope/today/" + signo;
    //return new Promise((resolve, reject) => {
    fetch(fetchUrl)
        .then(results => results.json())
        .then(horoscopeData => {
            console.log("Horoscope Data: " + horoscopeData);
            res.json({ ok: true, data: horoscopeData })
        })
        .catch(error => res.json({ ok: false, error }));
})

module.exports = router;