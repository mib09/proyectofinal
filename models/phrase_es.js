
'use strict';

//definición de modelo SEQUELIZE
module.exports = (sequelize, DataTypes) => {
  const Phrase_es = sequelize.define('Phrase_es', {
    // id (como primary key y autoinc), createdAt y updatedAt son campos que sequelize ya incluye por defecto
    frase: {
      type: DataTypes.STRING,      
      allowNull: false,
    },
    autor: {
      type: DataTypes.STRING,
      allowNull: false,

    },
    categoria: {
      type: DataTypes.INTEGER,
      allowNull: false,

    },
    puntuacionTotal: {
      type: DataTypes.INTEGER,
    },
    puntuado:{
      type: DataTypes.INTEGER,
    },
    reportes: {
      type: DataTypes.INTEGER,
    },
    
  }, { tableName: 'frases_es'});
  
  return Phrase_es;
};
