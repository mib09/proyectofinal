//importamos/requerimos express y controladores
const express = require('express');
const phrases_controller = require('./routes/phrases_controller');
const indexRouter = require('./routes/index_controller');
const horoscope_controller = require('./routes/horoscope_controller');
//instanciamos nueva aplicación express
const app = express();
//necesario para poder recibir datos en json
app.use(express.json());
//las ruta "/" se gestiona en indexRouter
app.use('/', indexRouter);
//las rutas que empiecen por /api/pokemons se dirigirán a pokemonsRouter
app.use('/api/phrases', phrases_controller);
app.use('/api/horoscope', horoscope_controller);
//arranque del servidor
const port = 3001
app.listen(port, () => console.log(`App listening on port ${port}!`))