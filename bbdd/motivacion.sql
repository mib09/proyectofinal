

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: motivacion
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `frases_en`
--

DROP TABLE IF EXISTS `frases_en`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frases_en` (
  `id` int(11) NOT NULL,
  `frase` varchar(45) DEFAULT NULL,
  `autor` varchar(45) DEFAULT NULL,
  `categoria` int(11) DEFAULT NULL,
  `puntuacion` varchar(45) DEFAULT NULL,
  `reportes` int(11) DEFAULT NULL,
  `createdAt` varchar(45) DEFAULT NULL,
  `updatedAt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frases_en`
--

LOCK TABLES `frases_en` WRITE;
/*!40000 ALTER TABLE `frases_en` DISABLE KEYS */;
INSERT INTO `frases_en` VALUES (1,'Just don\'t give up trying to do what you real','Ella Fitzgerald',0,'0',NULL,NULL,NULL),(2,'It is never too late to be what you might hav','George Eliot',0,'0',NULL,NULL,NULL),(3,'Inspiration comes from within yourself. One h','Deep Roy',0,'0',NULL,NULL,NULL),(4,'Never limit yourself because of others’ limit','Mae Jemison',0,'0',NULL,NULL,NULL),(5,'Be the change that you wish to see in the wor','Mahatma Gandhi',NULL,'0',NULL,NULL,NULL),(6,'Inspiration is some mysterious blessing which','Quentin Blake',NULL,'0',NULL,NULL,NULL),(7,'Keep your face to the sunshine and you cannot','Helen Keller',NULL,'0',NULL,NULL,NULL),(8,'Limit your \"always\" and your \"nevers.\"','Amy Poehler',NULL,'0',NULL,NULL,NULL),(9,'Spread love everywhere you go.','Mother Teresa',NULL,'0',NULL,NULL,NULL),(10,'A champion is defined not by their wins but b','Serena Williams',NULL,'0',NULL,NULL,NULL),(11,'Each person must live their life as a model f','Rosa Parks',NULL,'0',NULL,NULL,NULL),(12,'Motivation comes from working on things we ca','Sheryl Sandberg',NULL,'0',NULL,NULL,NULL),(13,'No matter what people tell you, words and ide','Robin Williams',NULL,'0',NULL,NULL,NULL),(14,'With the right kind of coaching and determina','Reese Witherspoon',NULL,'0',NULL,NULL,NULL),(15,'If you look at what you have in life, you\'ll ','Oprah Winfrey',NULL,'0',NULL,NULL,NULL),(16,'Life has got all those twists and turns. You\'','Nicole Kidman',NULL,'0',NULL,NULL,NULL),(17,'My mission in life is not merely to survive, ','Maya Angelou',NULL,'0',NULL,NULL,NULL),(18,'Let us make our future now, and let us make o','Malala Yousafzai',NULL,'0',NULL,NULL,NULL),(19,'Life changes very quickly, in a very positive','Lindsey Vonn',NULL,'0',NULL,NULL,NULL),(20,'You must do the things you think you cannot d','Eleanor Roosevelt',NULL,'0',NULL,NULL,NULL),(21,'Nothing is impossible. The word itself says \"','Audrey Hepburn',NULL,'0',NULL,NULL,NULL),(22,'Happiness is not by chance, but by choice.','Jim Rohn',NULL,'0',NULL,NULL,NULL),(23,'We must be willing to let go of the life we p','Joseph Campbell',NULL,'0',NULL,NULL,NULL),(24,'Don\'t wait. The time will never be just right','Napoleon Hill',NULL,'0',NULL,NULL,NULL),(25,'Some people look for a beautiful place. Other','Hazrat Inayat Khan',NULL,'0',NULL,NULL,NULL),(26,'Happiness often sneaks in through a door you ','John Barrymore',NULL,'0',NULL,NULL,NULL),(27,'Keep your face always toward the sunshine, an','Whalt Whitman',NULL,'0',NULL,NULL,NULL),(28,'You are never too old to set another goal or ','C.S. Lewis',NULL,'0',NULL,NULL,NULL),(29,'Life is like riding a bicycle. To keep your b','Albert Einstein',NULL,'0',NULL,NULL,NULL),(30,'Stay close to anything that makes you glad yo','Hafez',NULL,'0',NULL,NULL,NULL),(31,'It isn\'t where you came from. It\'s where you\'','Ella Fitzgerald',NULL,'0',NULL,NULL,NULL),(32,'Try to be a rainbow in someone else\'s cloud.','Maya Angelou',NULL,'0',NULL,NULL,NULL),(33,'You do not find the happy life. You make it.','Camilla Eyring Kimball',NULL,'0',NULL,NULL,NULL),(34,'The most wasted of days is one without laught','E.E.Cummings',NULL,'0',NULL,NULL,NULL),(35,'If I cannot do great things, I can do small t','Martin Luther King Jr.',NULL,'0',NULL,NULL,NULL),(36,'You don\'t always need a plan. Sometimes you j','Mandy Hale',NULL,'0',NULL,NULL,NULL),(37,'The bad news is time flies. The good news is ','Michael Altshuler',NULL,'0',NULL,NULL,NULL),(38,'Sometimes you will never know the value of a ','Dr. Seuss',NULL,'0',NULL,NULL,NULL),(39,'If you have good thoughts they will shine out','Roald Dahl',NULL,'0',NULL,NULL,NULL),(40,'Now go, and make interesting mistakes, make a','Neil Gaiman',NULL,'0',NULL,NULL,NULL),(41,'Be bold, be courageous, be your best.','Gabrielle Giffords',NULL,'0',NULL,NULL,NULL),(42,'To accomplish great things, we must not only ','Anatole France',NULL,'0',NULL,NULL,NULL),(43,'Start where you are. Use what you have. Do wh','Arthur Ashe',NULL,'0',NULL,NULL,NULL),(44,'Your life is your story, and the adventure ah','Kerry Washington',NULL,'0',NULL,NULL,NULL),(45,'Don’t be afraid of fear. Because it sharpens ','Ed Helms',NULL,'0',NULL,NULL,NULL),(46,'The only impossible journey is the one you ne','Anthony Robbins',NULL,'0',NULL,NULL,NULL),(47,'Take pride in how far you’ve come. Have faith','Michael Josephson',NULL,'0',NULL,NULL,NULL),(48,'If you want something you’ve never had, you m','Thomas Jefferson',NULL,'0',NULL,NULL,NULL),(49,'When you take risks, you learn that there wil','Ellen DeGeneres',NULL,'0',NULL,NULL,NULL),(50,'Go confidently in the direction of your dream','Henry David Thoreau',NULL,'0',NULL,NULL,NULL),(51,'Don\'t let the noise of others\' opinions drown','Steve Jobs',NULL,'0',NULL,NULL,NULL),(52,'If you can imagine it, you can achieve it; if','William Arthur Ward',NULL,'0',NULL,NULL,NULL),(53,'Every person you meet knows something you don','H.Jackson Brown Jr.',NULL,'0',NULL,NULL,NULL),(54,'Life is an improvisation. You have no idea wh','Stephen Colbert',NULL,'0',NULL,NULL,NULL),(55,'The future belongs to those who believe in th','Eleanor Roosevelt',NULL,'0',NULL,NULL,NULL),(56,'Fortune does favor the bold, and I promise th','Sheryl Sandberg',NULL,'0',NULL,NULL,NULL),(57,'It is impossible to live without failing at s','J.K. Rowling',NULL,'0',NULL,NULL,NULL),(60,'Walk down these crowded streets with a smile ','Zadie Smith',NULL,'0',NULL,NULL,NULL),(61,'It is not in the stars to hold our destiny bu','William Shakespeare',NULL,'0',NULL,NULL,NULL);
/*!40000 ALTER TABLE `frases_en` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frases_es`
--

DROP TABLE IF EXISTS `frases_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frases_es` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `frase` varchar(250) NOT NULL,
  `autor` varchar(45) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `puntuado` int(11) DEFAULT '1',
  `puntuacionTotal` int(11) DEFAULT '5',
  `reportes` int(11) NOT NULL,
  `createdAt` varchar(45) DEFAULT NULL,
  `updatedAt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`,`reportes`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frases_es`
--

LOCK TABLES `frases_es` WRITE;
/*!40000 ALTER TABLE `frases_es` DISABLE KEYS */;
INSERT INTO `frases_es` VALUES (1,'Just don\'t give up trying to do what you really want to do. Where there is love and inspiration, I don\'t think you can go wrong.','Ella Fitzgerald','',1,5,0,'',''),(2,'It is never too late to be what you might have been.','George Eliot','',1,5,0,'',''),(3,'Inspiration comes from within yourself. One has to be positive. When you\'re positive, good things happen.','Deep Roy','',1,5,0,'',''),(4,'Never limit yourself because of others’ limited imagination; never limit others because of your own limited imagination.','Mae Jemison','',1,5,0,'',''),(5,'Be the change that you wish to see in the world.','Mahatma Gandhi','',1,5,0,'',''),(6,'Inspiration is some mysterious blessing which happens when the wheels are turning smoothly.','Quentin Blake','',1,5,0,'',''),(7,'Keep your face to the sunshine and you cannot see a shadow.','Helen Keller','',1,5,0,'',''),(8,'Limit your \"always\" and your \"nevers.\"','Amy Poehler','',1,5,0,'',''),(9,'Spread love everywhere you go.','Mother Teresa','',1,5,0,'',''),(10,'A champion is defined not by their wins but by how they can recover when they fall.','Serena Williams','',1,5,0,'',''),(11,'Each person must live their life as a model for others.','Rosa Parks','',1,5,0,'',''),(12,'Motivation comes from working on things we care about.','Sheryl Sandberg','',1,5,0,'',''),(13,'No matter what people tell you, words and ideas can change the world.','Robin Williams','',1,5,0,'',''),(14,'With the right kind of coaching and determination you can accomplish anything.','Reese Witherspoon','',1,5,0,'',''),(15,'If you look at what you have in life, you\'ll always have more.','Oprah Winfrey','',1,5,0,'',''),(16,'Life has got all those twists and turns. You\'ve got to hold on tight and off you go.','Nicole Kidman','',1,5,0,'',''),(17,'My mission in life is not merely to survive, but to thrive.','Maya Angelou','',1,5,0,'',''),(18,'Let us make our future now, and let us make our dreams tomorrow\'s reality.','Malala Yousafzai','',1,5,0,'',''),(19,'Life changes very quickly, in a very positive way, if you let it.','Lindsey Vonn','',1,5,0,'',''),(20,'You must do the things you think you cannot do.','Eleanor Roosevelt','',1,5,0,'',''),(21,'Nothing is impossible. The word itself says \"I\'m possible!\"','Audrey Hepburn','',1,5,0,'',''),(22,'Happiness is not by chance, but by choice.','Jim Rohn','',1,5,0,'',''),(23,'We must be willing to let go of the life we planned so as to have the life that is waiting for us.','Joseph Campbell','',1,5,0,'',''),(24,'Don\'t wait. The time will never be just right.','Napoleon Hill','',1,5,0,'',''),(25,'Some people look for a beautiful place. Others make a place beautiful.','Hazrat Inayat Khan','',1,5,0,'',''),(26,'Happiness often sneaks in through a door you didn\'t know you left open.','John Barrymore','',1,5,0,'',''),(27,'Keep your face always toward the sunshine, and shadows will fall behind you.','Walt Whitman','',1,5,0,'',''),(28,'You are never too old to set another goal or to dream a new dream.','C.S. Lewis','',1,5,0,'',''),(29,'Life is like riding a bicycle. To keep your balance, you must keep moving.','Albert Einstein','',1,5,0,'',''),(30,'Stay close to anything that makes you glad you are alive.','Hafez','',1,5,0,'',''),(31,'It isn\'t where you came from. It\'s where you\'re going that counts.','Ella Fitzgerald','',1,5,0,'',''),(32,'Try to be a rainbow in someone else\'s cloud.','Maya Angelou','',1,5,0,'',''),(33,'You do not find the happy life. You make it.','Camilla Eyring Kimball','',1,5,0,'',''),(34,'The most wasted of days is one without laughter.','E.E. Cummings','',1,5,0,'',''),(35,'If I cannot do great things, I can do small things in a great way.','Martin Luther King Jr.','',1,5,0,'',''),(36,'You don\'t always need a plan. Sometimes you just need to breathe, trust, let go, and see what happens.','Mandy Hale','',1,5,0,'',''),(37,'The bad news is time flies. The good news is you\'re the pilot.','Michael Altshuler','',1,5,0,'',''),(38,'Sometimes you will never know the value of a moment, until it becomes a memory.','Dr. Seuss','',1,5,0,'',''),(39,'If you have good thoughts they will shine out of your face like sunbeams and you will always look lovely.','Roald Dahl','',1,5,0,'',''),(40,'Now go, and make interesting mistakes, make amazing mistakes, make glorious and fantastic mistakes. Break rules. Leave the world more interesting for you being here. Make good art.','Neil Gaiman','',1,5,0,'',''),(41,'Be bold, be courageous, be your best.','Gabrielle Giffords','',1,5,0,'',''),(42,'To accomplish great things, we must not only act, but also dream, not only plan, but also believe.','Anatole France','',1,5,0,'',''),(43,'Start where you are. Use what you have. Do what you can.','Arthur Ashe','',1,5,0,'',''),(44,'Your life is your story, and the adventure ahead of you is the journey to fulfill your own purpose and potential.','Kerry Washington','',1,5,0,'',''),(45,'Don’t be afraid of fear. Because it sharpens you, it challenges you, it makes you stronger; and when you run away from fear, you also run away from the opportunity to be your best possible self.','Ed Helms','',1,5,0,'',''),(46,'The only impossible journey is the one you never begin.','Anthony Robbins','',1,5,0,'',''),(47,'Take pride in how far you’ve come. Have faith in how far you can go. But don’t forget to enjoy the journey.','Michael Josephson','',1,5,0,'',''),(48,'If you want something you’ve never had, you must be willing to do something you’ve never done.','Thomas Jefferson','',1,5,0,'',''),(49,'When you take risks, you learn that there will be times when you succeed and there will be times when you fail, and both are equally important.','Ellen DeGeneres','',1,5,0,'',''),(50,'Go confidently in the direction of your dreams. Live the life you have imagined.','Henry David Thoreau','',1,5,0,'',''),(51,'Don\'t let the noise of others\' opinions drown out your own inner voice. And most important, have the courage to follow your heart and intuition.','Steve Jobs','',1,5,0,'',''),(52,'If you can imagine it, you can achieve it; if you can dream it, you can become it.','William Arthur Ward','',1,5,0,'',''),(53,'Every person you meet knows something you don’t; learn from them.','H.Jackson Brown Jr.','',1,5,0,'',''),(54,'Life is an improvisation. You have no idea what\'s going to happen next and you are mostly just making things up as you go along.','Stephen Colbert','',1,5,0,'',''),(55,'The future belongs to those who believe in the beauty of their dreams.','Eleanor Roosevelt','',1,5,0,'',''),(56,'Fortune does favor the bold, and I promise that you will never know what you’re capable of unless you try.','Sheryl Sandberg','',1,5,0,'',''),(57,'It is impossible to live without failing at something, unless you live so cautiously that you might as well not have lived at all.','J.K. Rowling','',1,5,0,'',''),(58,'Don’t ever confuse the two, your life and your work. The second is only part of the first.','Anna Quindlen','',1,5,0,'',''),(59,'Believe and act is if it were impossible to fail.','Charles Kettering','',1,5,0,'',''),(60,'Walk down these crowded streets with a smile on your face. Be thankful you get to walk so close to other humans.','Zadie Smith','',1,5,0,'',''),(61,'It is not in the stars to hold our destiny but in ourselves.','William Shakespeare','',1,5,0,'','');
/*!40000 ALTER TABLE `frases_es` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horoscopo`
--

DROP TABLE IF EXISTS `horoscopo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horoscopo` (
  `idhoroscopo` int(11) NOT NULL AUTO_INCREMENT,
  `acuario` varchar(45) DEFAULT NULL,
  `piscis` varchar(45) DEFAULT NULL,
  `aries` varchar(45) DEFAULT NULL,
  `tauro` varchar(45) DEFAULT NULL,
  `geminis` varchar(45) DEFAULT NULL,
  `cancer` varchar(45) DEFAULT NULL,
  `leo` varchar(45) DEFAULT NULL,
  `virgo` varchar(45) DEFAULT NULL,
  `libra` varchar(45) DEFAULT NULL,
  `escorpio` varchar(45) DEFAULT NULL,
  `sagitario` varchar(45) DEFAULT NULL,
  `carpicornio` varchar(45) DEFAULT NULL,
  `horoscopocol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idhoroscopo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horoscopo`
--

LOCK TABLES `horoscopo` WRITE;
/*!40000 ALTER TABLE `horoscopo` DISABLE KEYS */;
/*!40000 ALTER TABLE `horoscopo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `frase_add` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` varchar(45) DEFAULT NULL,
  `signo_horoscopo` varchar(45) DEFAULT NULL,
  `playlist` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idUsuarios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-21 12:08:40
